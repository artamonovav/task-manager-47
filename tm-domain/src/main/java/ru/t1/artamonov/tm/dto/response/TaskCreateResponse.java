package ru.t1.artamonov.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.dto.model.TaskDTO;

@NoArgsConstructor
public class TaskCreateResponse extends AbstractTaskResponse {

    public TaskCreateResponse(@Nullable final TaskDTO task) {
        super(task);
    }

}
