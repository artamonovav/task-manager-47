package ru.t1.artamonov.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.dto.model.ProjectDTO;

@NoArgsConstructor
public final class ProjectCreateResponse extends AbstractProjectResponse {

    public ProjectCreateResponse(@Nullable final ProjectDTO project) {
        super(project);
    }

}
