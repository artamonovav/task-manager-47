package ru.t1.artamonov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.api.repository.dto.ISessionDTORepository;
import ru.t1.artamonov.tm.dto.model.SessionDTO;

import javax.persistence.EntityManager;
import java.util.List;

public interface ISessionDTOService {

    @NotNull
    ISessionDTORepository getRepository(@NotNull EntityManager entityManager);

    @NotNull
    SessionDTO add(@Nullable SessionDTO model);

    @NotNull
    SessionDTO add(@Nullable String userId, @Nullable SessionDTO model);

    @NotNull
    List<SessionDTO> add(@NotNull List<SessionDTO> models);

    void clear(@NotNull String userId);

    boolean existsById(@NotNull String id);

    @Nullable
    List<SessionDTO> findAll();

    @Nullable
    SessionDTO findOneById(@Nullable String id);

    @Nullable
    SessionDTO findOneById(@Nullable String userId, @Nullable String id);

    @NotNull
    SessionDTO remove(@Nullable SessionDTO model);

    @NotNull
    SessionDTO removeById(@Nullable String id);

    @NotNull
    SessionDTO removeById(@Nullable String userId, @Nullable String id);

    @NotNull
    SessionDTO update(@Nullable SessionDTO model);

}
