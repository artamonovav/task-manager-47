package ru.t1.artamonov.tm.api.repository.model;

import ru.t1.artamonov.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

}
