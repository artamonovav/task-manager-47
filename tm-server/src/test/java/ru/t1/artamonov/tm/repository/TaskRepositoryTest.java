//package ru.t1.artamonov.tm.repository;
//
//import lombok.SneakyThrows;
//import org.jetbrains.annotations.NotNull;
//import org.junit.After;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.experimental.categories.Category;
//import ru.t1.artamonov.tm.api.repository.ITaskRepository;
//import ru.t1.artamonov.tm.api.service.IConnectionService;
//import ru.t1.artamonov.tm.marker.UnitCategory;
//import ru.t1.artamonov.tm.dto.model.Task;
//import ru.t1.artamonov.tm.service.ConnectionService;
//import ru.t1.artamonov.tm.service.PropertyService;
//
//import java.sql.Connection;
//
//import static ru.t1.artamonov.tm.constant.ProjectTestData.USER1_PROJECT1;
//import static ru.t1.artamonov.tm.constant.TaskTestData.*;
//import static ru.t1.artamonov.tm.constant.UserTestData.USER1;
//import static ru.t1.artamonov.tm.constant.UserTestData.USER2;
//
//@Category(UnitCategory.class)
//public final class TaskRepositoryTest {
//
//    @NotNull
//    private final PropertyService propertyService = new PropertyService();
//
//    @NotNull
//    private final IConnectionService connectionService = new ConnectionService(propertyService);
//
//    @NotNull
//    private final Connection connection = connectionService.getConnection();
//
//    @NotNull
//    ITaskRepository taskRepository = new TaskRepository(connection);
//
//    @After
//    @SneakyThrows
//    public void dropConnection() {
//        connection.rollback();
//        connection.close();
//    }
//
//    @Before
//    public void init() {
//        taskRepository.clear();
//    }
//
//    @Test
//    public void add() {
//        Assert.assertTrue(taskRepository.findAll().isEmpty());
//        taskRepository.add(USER1_TASK1);
//        Assert.assertEquals(USER1_TASK1.getId(), taskRepository.findAll().get(0).getId());
//    }
//
//    @Test
//    public void addByUserId() {
//        Assert.assertTrue(taskRepository.findAll().isEmpty());
//        taskRepository.add(USER1.getId(), USER1_TASK1);
//        Task task = taskRepository.findAll().get(0);
//        Assert.assertEquals(USER1_TASK1.getId(), taskRepository.findAll().get(0).getId());
//        Assert.assertEquals(USER1.getId(), taskRepository.findAll().get(0).getUserId());
//    }
//
//    @Test
//    public void clearByUserId() {
//        Assert.assertTrue(taskRepository.findAll().isEmpty());
//        taskRepository.add(USER1_TASK_LIST);
//        taskRepository.clear(USER2.getId());
//        Assert.assertFalse(taskRepository.findAll().isEmpty());
//        taskRepository.clear(USER1.getId());
//        Assert.assertTrue(taskRepository.findAll().isEmpty());
//        taskRepository.add(USER2_TASK1);
//        taskRepository.clear(USER1.getId());
//        Assert.assertFalse(taskRepository.findAll().isEmpty());
//    }
//
//    @Test
//    public void findAllByUserId() {
//        Assert.assertTrue(taskRepository.findAll().isEmpty());
//        taskRepository.add(TASK_LIST);
//        Assert.assertEquals(USER1_TASK_LIST.size(), taskRepository.findAll(USER1.getId()).size());
//    }
//
//    @Test
//    public void findOneByIdByUserId() {
//        Assert.assertTrue(taskRepository.findAll().isEmpty());
//        taskRepository.add(USER1_TASK1);
//        Assert.assertEquals(USER1_TASK1.getId(), taskRepository.findOneById(USER1.getId(), USER1_TASK1.getId()).getId());
//    }
//
//    @Test
//    public void removeByUserId() {
//        Assert.assertTrue(taskRepository.findAll().isEmpty());
//        taskRepository.add(USER1_TASK1);
//        taskRepository.add(USER2_TASK1);
//        Assert.assertEquals(USER1_TASK1.getId(), taskRepository.remove(USER1.getId(), USER1_TASK1).getId());
//        Assert.assertEquals(1, taskRepository.findAll().size());
//    }
//
//    @Test
//    public void removeByIdByUserId() {
//        Assert.assertTrue(taskRepository.findAll().isEmpty());
//        taskRepository.add(USER1_TASK1);
//        taskRepository.add(USER2_TASK1);
//        Assert.assertEquals(USER1_TASK1.getId(), taskRepository.removeById(USER1.getId(), USER1_TASK1.getId()).getId());
//        Assert.assertEquals(1, taskRepository.findAll().size());
//    }
//
//    @Test
//    public void existsByIdByUserId() {
//        Assert.assertTrue(taskRepository.findAll().isEmpty());
//        taskRepository.add(USER1_TASK1);
//        Assert.assertTrue(taskRepository.existsById(USER1_TASK1.getId()));
//        Assert.assertFalse(taskRepository.existsById(USER2_TASK1.getId()));
//    }
//
//    @Test
//    public void findAllByProjectIdByUserId() {
//        Assert.assertTrue(taskRepository.findAll().isEmpty());
//        taskRepository.add(USER1_TASK_LIST);
//        Assert.assertEquals(USER1_TASK_LIST.size(), taskRepository.findAllByProjectId(USER1.getId(), USER1_PROJECT1.getId()).size());
//    }
//
//}
